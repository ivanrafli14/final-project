<nav class="navbar navbar-expand-md navbar-light border-bottom p-0 ps-5">
    <div class="container">

        <a class="navbar-brand" href="#">
            <span class="text-main-color fw-bold fs-3">Final Project</span>

        <img src="{{asset('src/logo.png')}}" width="40" height="40">
        <a class="navbar-brand" href="/dashboard">
            <span class="text-main-color fw-bold fs-3 ml-3"><strong>Laforum</strong></span>

        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ">
                <li class="nav-item border-main-color">
                    <a class="nav-link" href="/dashboard">
                        <svg class="text-main-color" xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 24 24" fill="currentColor"><path d="M0 0h24v24H0z" fill="none"/><path d="M10 20v-6h4v6h5v-8h3L12 3 2 12h3v8z"/></svg>                       
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <svg class="text-muted" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" viewBox="0 0 24 24" fill="currentColor"><g><path d="M0,0h24v24H0V0z" fill="none"/></g><g><path d="M16,3H5C3.9,3,3,3.9,3,5v14c0,1.1,0.9,2,2,2h14c1.1,0,2-0.9,2-2V8L16,3z M19,19H5V5h10v4h4V19z M7,17h10v-2H7V17z M12,7H7 v2h5V7z M7,13h10v-2H7V13z"/></g></svg>
                        
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <svg class="text-muted" xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24"  viewBox="0 0 24 24" fill="currentColor"><rect fill="none" height="24" width="24"/><path d="M3,10h11v2H3V10z M3,8h11V6H3V8z M3,16h7v-2H3V16z M18.01,12.87l0.71-0.71c0.39-0.39,1.02-0.39,1.41,0l0.71,0.71 c0.39,0.39,0.39,1.02,0,1.41l-0.71,0.71L18.01,12.87z M17.3,13.58l-5.3,5.3V21h2.12l5.3-5.3L17.3,13.58z"/></svg>
                        <span class="position-relative">
                            
                        </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">
                        <svg class="text-muted" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M9 13.75c-2.34 0-7 1.17-7 3.5V19h14v-1.75c0-2.33-4.66-3.5-7-3.5zM4.34 17c.84-.58 2.87-1.25 4.66-1.25s3.82.67 4.66 1.25H4.34zM9 12c1.93 0 3.5-1.57 3.5-3.5S10.93 5 9 5 5.5 6.57 5.5 8.5 7.07 12 9 12zm0-5c.83 0 1.5.67 1.5 1.5S9.83 10 9 10s-1.5-.67-1.5-1.5S8.17 7 9 7zm7.04 6.81c1.16.84 1.96 1.96 1.96 3.44V19h4v-1.75c0-2.02-3.5-3.17-5.96-3.44zM15 12c1.93 0 3.5-1.57 3.5-3.5S16.93 5 15 5c-.54 0-1.04.13-1.5.35.63.89 1 1.98 1 3.15s-.37 2.26-1 3.15c.46.22.96.35 1.5.35z"/></svg>                        </a>
                </li>
                {{-- <li class="nav-item">
                    <a class="nav-link" href="#">
                        <svg class="text-muted" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M12 22c1.1 0 2-.9 2-2h-4c0 1.1.9 2 2 2zm6-6v-5c0-3.07-1.63-5.64-4.5-6.32V4c0-.83-.67-1.5-1.5-1.5s-1.5.67-1.5 1.5v.68C7.64 5.36 6 7.92 6 11v5l-2 2v1h16v-1l-2-2zm-2 1H8v-6c0-2.48 1.51-4.5 4-4.5s4 2.02 4 4.5v6z"/></svg>                        </a>
                </li> --}}
                
            </ul>

            <form action="/dashboard/search" class="d-flex">
                
                <input class="form-control me-2  search-icon"  id="navBarSearchForm" type="search" placeholder="Search Question" aria-label="Search" style="width: 456px" name="search">

        <div class="collapse navbar-collapse" id="navbarToggleExternalContent">
        

            <form action="/dashboard/search" class="d-flex">
                
                <input class="form-control me-2  search-icon"  id="navBarSearchForm" type="search" placeholder="Search" aria-label="Search" style="width: 600px" name="search">

                
            </form>

            

            <ul class="navbar-nav px-3 ">
                <li class="nav-item mt-2">
                    @auth
                        <div class="d-flex user-logged nav-item dropdown no-arrow">
                            
                                    <img src="{{asset('storage/'. Auth::user()->avatar)}}" class="user-photo  profile rounded-circle" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            
                    
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                
                            <a class="dropdown-item" href="/user/{{Auth::user()->id}}">My Profile  </a>
                            <a href="#" class="dropdown-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Sign Out</a>
                            <form id = "logout-form" action="{{route('logout')}}" method = "POST" style="display: none">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                            </form>
                            </div>
                        </div>
                


                


                </li>
                
                {{-- <li class="nav-item">
                    <a class="nav-link" href="#">
                        <svg class="text-muted" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M11.99 2C6.47 2 2 6.48 2 12s4.47 10 9.99 10C17.52 22 22 17.52 22 12S17.52 2 11.99 2zm6.93 6h-2.95c-.32-1.25-.78-2.45-1.38-3.56 1.84.63 3.37 1.91 4.33 3.56zM12 4.04c.83 1.2 1.48 2.53 1.91 3.96h-3.82c.43-1.43 1.08-2.76 1.91-3.96zM4.26 14C4.1 13.36 4 12.69 4 12s.1-1.36.26-2h3.38c-.08.66-.14 1.32-.14 2s.06 1.34.14 2H4.26zm.82 2h2.95c.32 1.25.78 2.45 1.38 3.56-1.84-.63-3.37-1.9-4.33-3.56zm2.95-8H5.08c.96-1.66 2.49-2.93 4.33-3.56C8.81 5.55 8.35 6.75 8.03 8zM12 19.96c-.83-1.2-1.48-2.53-1.91-3.96h3.82c-.43 1.43-1.08 2.76-1.91 3.96zM14.34 14H9.66c-.09-.66-.16-1.32-.16-2s.07-1.35.16-2h4.68c.09.65.16 1.32.16 2s-.07 1.34-.16 2zm.25 5.56c.6-1.11 1.06-2.31 1.38-3.56h2.95c-.96 1.65-2.49 2.93-4.33 3.56zM16.36 14c.08-.66.14-1.32.14-2s-.06-1.34-.14-2h3.38c.16.64.26 1.31.26 2s-.1 1.36-.26 2h-3.38z"/></svg>          
                    </a>
                </li> --}}

                </li>
                
                <li class="nav-item">
                    
                        <p class=" mt-3">Hai, {{ Auth::user()->name }}</p>
                </li>


            </ul>

             

             
                
                <a type="button" class="btn bg-main-color text-light" href="/post/{{ Auth::user()->id }}">Add Question</a>
                @endauth
                @guest
                <a href="/login" class="btn bg-main-color text-light mb-2 ml-4 mr-2 " style="border: 5px solid #E7E5F4;
                border-radius: 50px;">Login</a>
                <a class="btn btn-border btn-google-login text-dark mb-2 ml-2" href="sign-in-google" style="border: 5px solid #E7E5F4;
                border-radius: 50px;
                ">
                    <img src="{{asset('src/ic_google.svg')}}" class="icon" alt=""> Sign In with Google
                </a>
                @endguest
        </div>
        
    </div>
</nav>