## Final Project

## Kelompok 5

## Anggota Kelompok

-   Adi Irawan (adiirwn)
-   Ivan Rafli Adipratama (IvanRaf14)
-   Rianti Ruth Florenza Tambunan (ruthflorenza)

## Tema Project

Forum Tanya Jawab

## ERD

<img src="/public/src/erd.jpeg" alt="Build Status">

## Link Video

-   Link Demo Aplikasi : https://youtu.be/-NQzyGGnRlI </br>
-   Link Diploy : http://final.laforum.sanbercodeapp.com/dashboard
